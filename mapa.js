
let miMapa = L.map('mapid');

miMapa.setView([4.6500027,-74.170230], 16);

let miProveedor= L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
});
miProveedor.addTo(miMapa);

let miMarcador = L.marker([4.650783,-74.173258]);
miMarcador.addTo(miMapa)

var circle = L.circle([4.650002773,-74.17023056], {
    color: 'blue',
    fillColor: 'purple',
    fillOpacity: 0.5,
    radius: 460
});
circle.addTo(miMapa);

var polygon = L.polygon([
    [4.65091, -74.17303],
    [4.65133, -74.17251],
    [4.65227, -74.17333],
    [4.65205, -74.17390]
],{
    color:'yellow'
});
polygon.addTo(miMapa)